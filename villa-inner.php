<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Mangalyam Meadows - Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="http://togglehead.net/mangalyam-micro//images/favicon.png" type="image/svg" sizes="16x16">

    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick-theme.css" />

    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
  </head>
  <body class="static">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <?php include 'includes/header.php'; ?>
    <!--main-container-->
    <main class="main-container full-h">

      <!--Sticky-->
        <?php include 'includes/sticky.php'; ?>
      <!--Sticky end-->
      <div class="container"><?php include 'includes/breadcrumb.php'; ?></div>

        <section class="inner-banner full-h">
            <img src="https://dummyimage.com/992x550/" alt="" class="mob-tab"/>
            <!--single-content-->
            <div class="single-content">
                <div class="flex full-h">
                    
                    <!--box-->
                    <div class="single-dbox desk-tab">
                        <!--name-->
                        <div class="single-name">
                          <img src="img/home/our-villas/fernvilla.svg" alt=""/>
                        </div>
                        <!--name end-->

                        <!--divider-->
                        <div class="single-divider">
                          <img src="img/home/our-villas/villa-divider.svg" alt=""/>
                        </div>
                        <!--divider end-->

                        <!--single-tag-->
                        <div class="single-tag">
                          <h3 class="text-center text-uppercase">It's a palace, your Hightness!</h3>
                        </div>
                        <!--single-yag end-->

                        <!--detail-->
                        <div class="single-detail">
                          <p>5 Bed  |  G + 2  |  4640 Sq. Ft.</p>
                        </div>
                        <!--detail end-->

                        <div class="single-btn">
                          <button class="cta">Explore More</button>
                        </div>
                    </div>
                    <!--box end-->
                </div>
            </div>
            <!--single-content end--> 
            
            <div class="banner-strip">
                <div class="container">
                <ul class="flex">
                    <li><p>5 Bed</p></li>  <li><p>|</p></li>  <li><p>G + 2</p></li>  <li><p>|</p></li>  <li><p>4640 Sq. Ft.</p></li>
                    <li>df</li>
                    <li>fdfd</li>
                </ul>
                </div>
            </div>
            
        </section>

    </main> 
    <!--main-container end-->

    <?php include 'includes/footer.php'; ?>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                                                              function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                             e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                             e.src='//www.google-analytics.com/analytics.js';
                             r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
  </body>
</html>

<div class="modal fade enquire-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <!-- Modal Content: begins -->
  <div class="modal-content text-center">   
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <!-- Modal Header -->
    <div class="modal-header justify-content-center">
        <h4 class="modal-title text-uppercase">Enquire Now</h4>
    </div>
  
    <!-- Modal Body -->  
    <div class="modal-body">
      
      <div class="body-message">
        <ul id="myTabs" class="nav nav-pills nav-justified" role="tablist" data-tabs="tabs">
          <li class="active col-md-6"><a href="#message" data-toggle="tab" class="active">Write A Message</a></li>
          <li class="col-md-6"><a href="#call" data-toggle="tab">Call Now</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active show" id="message">
            <?php include 'form.php'; ?>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="call">
            <p>Call Mangalyam Meadows</p>
            <h3 class="call"><a href="tel:+91 8655094922">+91 8655094922</a></h3>
          </div>
        </div>
      </div>

    </div>  
  </div>
  <!-- Modal Content: ends -->
</div>        
</div>



<div class="modal fade book-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <!-- Modal Content: begins -->
  <div class="modal-content">
    
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <!-- Modal Header -->
    
    <div class="modal-header justify-content-center mob-tab">
        <h4 class="modal-title text-uppercase">Book a visit</h4>
    </div>  
    <!-- Modal Body -->  
    <div class="modal-body">
      <div class="body-subtitle">
      <?php include 'book.php'; ?>
      </div>
      <div class="body-message">
      
      </div>
    </div>
  
  </div>
  <!-- Modal Content: ends -->
  
</div>

</div>



<div class="modal fade download-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <!-- Modal Content: begins -->
  <div class="modal-content text-center">
    
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <!-- Modal Header -->
    <div class="modal-header justify-content-center">
        <h4 class="modal-title text-uppercase">Download Brochure</h4>
    </div>
  
    <!-- Modal Body -->  
    <div class="modal-body">
      <div class="body-message contact-form">
        <form action="" name="form" method="post" id="download">
          <div class="row">
              <div class="col-md-12">
                  <div class="input-group">
                      <label for="email">Email ID*</label>
                      <input type="email" name="email" id="email" required/>
                      <button type="submit"></button>
                  </div>
              </div>
          </div>
        </form>
      </div>
    </div>
  
  </div>
  <!-- Modal Content: ends -->
  
</div>

</div>
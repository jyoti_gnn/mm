<header>
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="#"><img class="white" src="img/logo-white.png">
        <img class="color" src="img/logo-green.svg"></a>

        <button class="hamburger navbar-toggler" id="hamburger-6" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="line"></span>
          <span class="line"></span>
          <span class="line"></span>
        </button>

        <!-- <button class="navbar-toggler" >
        <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto first">

                <li class="nav-item parent-drop">
                    <a class="nav-link text-uppercase" href="javascript:void(0)">Villas</a>
                    <i class="fa fa-angle-down"></i>
                    <ul class="dropdown">
                        <li><a class="nav-link text-uppercase" href="">Fern Villas<i class="fa fa-chevron-right"></i></a></li>
                        <li><a class="nav-link text-uppercase" href="">Orchard Villas<i class="fa fa-chevron-right"></i></a></li>
                        <li><a class="nav-link text-uppercase" href="">Spring Villas<i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">Clubhouse</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">Luxury That Pays</a>
                </li>

            </ul>
            <ul class="navbar-nav ml-auto second">

                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">Location</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">Contact Us</a>
                </li>

            </ul>
        </div>
    </nav>
    </div>
</header>

<div class="row">
    <div class="col-lg-3">
        <div class="bav_left_box d-none d-lg-block">
            <h4 class="section_title text-uppercase">Book A Visit</h4>
            <p class="bav_desc">Visit us to have a first-hand experience in luxury and know more on how luxury can pay you. Book now to schedule a meeting.</p>
            <p class="bav_address">Mangalyam Meadows, Chanvai Rabda Road, Chanvai Village, District : Valsad, Gujarat 396020</p>
            <p class="bav_time">1 hour</p>
        </div>
    </div>
    <div class="col-lg-9 p-0">
        <div class="flex bav_right_box">
            <div class="bav_date_box bav_dt_box pad-0">
                <label for="date">Select Date:</label>
                <div id="datepicker"></div>
            </div>
            <div class="bav_time_box bav_dt_box pad-0">
                <label for="time">Select Time Slot:</label>
                <ul class="selectDate">
                    <li data-attr="09:00 am - 10:00 am" class="text-center disable">09:00 AM - 10:00 AM</li>
                    <li data-attr="09:30 am - 10:30 am" class="text-center disable">09:30 AM - 10:30 AM</li>
                    <li data-attr="10:00 am - 11:00 am" class="text-center disable">10:00 AM - 11:00 AM</li>
                    <li data-attr="10:30 am - 11:30 am" class="text-center disable">10:30 AM - 11:30 AM</li>
                    <li data-attr="11:00 am - 12:00 pm" class="text-center disable">11:00 AM - 12:00 PM</li>
                    <li data-attr="11:30 am - 12:30 pm" class="text-center disable">11:30 AM - 12:30 PM</li>
                    <li data-attr="12:00 pm - 01:00 pm" class="text-center disable">12:00 PM - 01:00 PM</li>
                    <li data-attr="12:30 pm - 01:30 pm" class="text-center disable">12:30 PM - 01:30 PM</li>
                    <li data-attr="01:00 pm - 02:00 pm" class="text-center disable">01:00 PM - 02:00 PM</li>
                    <li data-attr="01:30 pm - 02:30 pm" class="text-center disable">01:30 PM - 02:30 PM</li>
                    <li data-attr="02:00 pm - 03:00 pm" class="text-center disable">02:00 PM - 03:00 PM</li>
                    <li data-attr="02:30 pm - 03:30 pm" class="text-center disable">02:30 PM - 03:30 PM</li>
                    <li data-attr="03:00 pm - 04:00 pm" class="text-center disable">03:00 PM - 04:00 PM</li>
                    <li data-attr="03:30 pm - 04:30 pm" class="text-center disable">03:30 PM - 04:30 PM</li>
                    <li data-attr="04:00 pm - 05:00 pm" class="text-center disable">04:00 PM - 05:00 PM</li>
                </ul>
                <div class="text-center">
                    <button id="confirmSlot" class="cta confirm mangalyam_cta removePointer">Confirm</button>
                </div>
            </div>
            <div class="contact-form bav_form_box">
                <div class="bav_form_inner_box"> 
                    <a class="back_btn"> <img src="<?php echo $base_url; ?>images/arrow-back.svg" class="back_icon">Back</a>
                    <p class="print-data">
                        <span class="print-time"></span>,<span class="print-date"></span>
                    </p>
                    <h3 class="section_title">Enter Details</h3>
                    <form id="bookavisit" method="POST" novalidate>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" id="bavname" name="bavname" required>
                                <label for="name">Name*</label>
                            </div>
                            <span class="bavname"></span>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" id="bavemail" name="bavemail" required>
                                <label for="email">Email*</label>
                            </div>
                            <span class="bavemail"></span>
                        </div>
                        <div class="form-group">
                            <div class="input-group bavphone_field">
                                <label for="bavphone">Phone Number*</label>
                                <input id="bavphone" name="bavphone" type="text" required onkeypress="return isNumberKey(event)">
                            </div>
                            <span class="bavphone"></span>
                        </div>
                        <button type="submit" class="cta">SCHEDULE EVENT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
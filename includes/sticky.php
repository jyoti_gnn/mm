<div class="sticky-content">
    <div class="enquire rotate">
        <h3 class="text-uppercase" data-toggle="modal" data-target=".enquire-modal">Enquire Now</h3>
    </div>
    <div class="book rotate">
        <h3 class="text-uppercase" data-toggle="modal" data-target=".book-modal">Book a visit</h3>
    </div>
    <div class="download">
        <h3 data-toggle="modal" data-target=".download-modal"><span></span></h3>
    </div>
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=+918369997446&text=Please%20tell%20me%20more%20about%20Mangalyam%20Meadows" target="_blank">
            <span></span>
        </a>
    </div>
</div>

<?php include 'popup.php'; ?>
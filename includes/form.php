<div class="contact-form">
    <form action="" name="form" method="post" id="form">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" required/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" required/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <label for="phone">Phone Number</label>
                    <input type="tel" name="phone" id="phone" onkeypress="return event.charCode >=48 && event.charCode<=57" required/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <label for="city">City</label>
                    <input type="text" name="city" id="city" required/>
                </div>
            </div>   
            <div class="col-md-12">
                <div class="input-group">
                    <label for="message">Message</label>
                    <textarea name="message" id="message" required></textarea>
                </div>
            </div>  
            <div class="col-md-12">
                <div class="single-btn">
                  <button type="submit" class="cta text-uppercase">Enquire Now</button>
                </div>
            </div>       
        </div>
    </form>
</div>
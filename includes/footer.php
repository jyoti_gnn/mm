<footer class="footer-container">
    <div class="footer-bg">
        <div class="container">
            <div class="footer-wrap row">
                <div class="col-md-3 col-sm-6">
                    <div class="row">
                        <div class="col-md-6 col-6">
                            <img src="img/mm-logo.svg"/>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <div class="line"></div>
                            <img src="img/dgip-logo.svg"/>
                        </div>
                        <div class="partners-box">
                            <div class="col-md-12">
                                <h4>Strategic consultants :</h4>
                                <div class="logo-bx">
                                    <img src="img/edelweiss-logo.svg"/>
                                </div>
                            </div>
                        </div>

                        <div class="partners-box">
                            <div class="col-md-12">
                                <h4>Project managed by :</h4>
                                <div class="logo-bx">
                                    <img src="img/tripvilla-logo.svg"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h3 class="foot-title">Mumbai Office</h3>
                    <div class="tab-content">
                        <div class="office-box">
                            <p>Aum Jaydeep Heights, 2nd Flr,<br>N. S. Rd No. 1, Vile Parle (W),<br>Mumbai – 400056</p>
                        </div>
                        <div class="office-box">
                            <ul>
                                <li>Mr. Mehul : <a href="tel:+91 8655094922">+91 8655094922</a></li>
                                <li>Mr. Shishir : <a href="tel:+91 9029094922">+91 9029094922</a></li>
                            </ul>
                        </div>
                        <div class="office-box">
                            <p><a href="tel:+91 8655094922">Get Google Location</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h3 class="foot-title">Site Office</h3>
                    <div class="tab-content">
                    <div class="office-box">
                        <p>Mangalyam Meadows,<br>Chanvai Rabda Road,<br>Chanvai Village, District:Valsad,<br>Gujarat 396020</p>
                    </div>
                    <div class="office-box">
                        <ul>
                            <li>Mr. Amit : <a href="tel:+91 9328194922">+91 9328194922</a></li>
                            <li>Mr. Manish : <a href="tel:+91 9327294922">+91 9327294922</a></li>
                        </ul>
                    </div>
                    <div class="office-box">
                        <p><a href="tel:+91 8655094922">Get Google Location</a></p>
                    </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h3 class="foot-title last-accordian">Clubhouse Enquiries</h3>
                    <div class="tab-content">
                        <div class="office-box">
                            <ul>
                                <li><a href="tel:+91 8928750330">+91 8928750330</a></li>
                                <li><a href="tel:+91 2632297021">+91 2632297021</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="office-box last-child">
                        <h3 class="foot-title">Request a Call Back</h3>
                        <form action="" name="subs" method="post" id="subs">
                            <div class="subscribe-box">
                                <div class="input-prefix">
								    <span class="input-text">+91</span>
								</div>
                                <input type="tel" name="phone" id="phone" class="form-control" onkeypress="return event.charCode >=48 && event.charCode<=57" required/>
                                <div class="input-suffix">
									<button class="btn" type="submit">
										<img src="http://togglehead.net/mangalyam-micro/images/racb.svg" alt=""/>
									</button>
								</div>
                            </div>
                        </form>
                        <div class="office-box social-icons">
                            <ul>
                                <li><a href=""><img src="img/fb.svg" alt=""/></a></li>
                                <li><a href=""><img src="img/insta.svg" alt=""/></a></li>
                                <li><a href=""><img src="img/yt.svg" alt=""/></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
		<div class="container">
			<p>Copyright © 2020 All Rights Reserved by Mangalyam Meadows.</p>
		</div>
	</div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="js/validation.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
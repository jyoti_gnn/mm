<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Mangalyam Meadows - Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,viewport-fit=cover,user-scalable=0"/>
    <meta name=”theme-color” content=”#A38067”>
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="http://togglehead.net/mangalyam-micro//images/favicon.png" type="image/svg" sizes="16x16">

    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick-theme.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <?php include 'includes/header.php'; ?>
    <!--main-container-->
    <main class="main-container">
     
        <section class="banner">
          <h2 class="text-uppercase banner-text text-center">Gateway to Fine Living</h2>
          
          <!--Sticky-->
          <?php include 'includes/sticky.php'; ?>
          <!--Sticky end-->

          <div class="scrollto">
            <div class="box">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
        </section>
        
        <section id="scroll-sect"></section>
        <section class="section-divide">
          <div class="section-wrapper">
            <div class="quote-wrap">
              <h3 class="text-center">Experience luxury and all the comforts of modern living in the midst of nature in Valsad.</h3>
            </div>
          </div>
        </section>

        <section class="section-divide">
          <div class="full-width video-sect">
            <div class="Responsive-Ratio Dynamic Fit-Content video_wrapper video_wrapper_full js-videoWrapper">
              <video width="100%" height="935" controls="" id="player1" class="videoIframe js-videoIframe">
                <source src="movie.mp4" type="video/mp4">
                <source src="movie.ogg" type="video/ogg">
                Your browser does not support the video tag.
              </video>
              <!-- <iframe id="player1" class="videoIframe js-videoIframe" src="https://www.youtube.com/embed/mxYNdy-0CEs?rel=0&modestbranding=1&autohide=1&showinfo=0&autoplay=1&mute=1&controls=0" width="720" height="400" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allowTransparency="true" data-src="https://www.youtube.com/embed/mxYNdy-0CEs?rel=0&modestbranding=1&autohide=1&showinfo=0&autoplay=1&mute=1&controls=0"></iframe> -->
              <button class="videoPoster js-videoPoster"></button>
            </div>
              <div class="club-box">
                <div class="single-dbox">
                  <div class="single-tag">
                    <img src="img/curve-left.svg" alt=""/>
                    <h3 class="text-uppercase">You're one</h3>
                    <img src="img/curve-right.svg" alt=""/>
                  </div>
                  <div class="single-divider">
                    <h2 class="text-uppercase">Decision</h2>
                  </div>
                  <div class="single-tag">
                    <h3 class="text-center text-uppercase">Away from a signature</h3>
                  </div>
                  <div class="single-divider">
                    <h2 class="text-uppercase">Lifestyle</h2>
                  </div>
                  <div class="single-btn">
                    <a class="cta" tabindex="0">Explore More<i class="fa fa-chevron-right"></i></a>
                  </div>
                </div>
              </div>
            
          </div>
        </section>

        <section class="section-divide">
          <div class="section-wrapper">
            <div class="sect-title text-center">
              <h2 class="text-uppercase">Our Limited Edition Villas</h2>
              <p>We've rethought fine living for a truly signature lifestyle.<br> Our villas are the true definition of luxury living.</p>
            </div>
            <div class="container limited">
              <div class="single-item">

                <!--Looping-->
                <div class="single-wrap">
                  <div class="single-image">
                    <img src="img/home/our-villas/fernvilla.jpg" class="desk-img" alt=""/>
                    <img src="https://dummyimage.com/768x500/" class="mob-img" alt=""/>
                  </div>
                  <!--single-content-->
                  <div class="single-content">
                    <div class="flex full-h">
                    
                      <!--box-->
                      <div class="single-dbox">
                        <!--name-->
                        <div class="single-name">
                          <img src="img/home/our-villas/fernvilla.svg" alt=""/>
                        </div>
                        <!--name end-->

                        <!--divider-->
                        <div class="single-divider">
                          <img src="img/home/our-villas/villa-divider.svg" alt=""/>
                        </div>
                        <!--divider end-->

                        <!--single-tag-->
                        <div class="single-tag">
                          <h3 class="text-center text-uppercase">It's a palace, your Hightness!</h3>
                        </div>
                        <!--single-yag end-->

                        <!--detail-->
                        <div class="single-detail">
                          <p>5 Bed  |  G + 2  |  4640 Sq. Ft.</p>
                        </div>
                        <!--detail end-->

                        <div class="single-btn">
                          <a class="cta">Explore More<i class="fa fa-chevron-right"></i></a>
                        </div>
                      </div>
                      <!--box end-->
                    
                    </div>
                  </div>
                  <!--single-content end-->
                </div>
                <!--Looping end-->

                <div class="single-wrap">
                  <div class="single-image">
                    <img src="img/home/our-villas/fernvilla.jpg" class="desk-img" alt=""/>
                    <img src="https://dummyimage.com/768x500/" class="mob-img" alt=""/>
                  </div>
                  <!--single-content-->
                  <div class="single-content">
                    <div class="flex full-h">
                    
                      <!--box-->
                      <div class="single-dbox">
                        <!--name-->
                        <div class="single-name">
                          <img src="img/home/our-villas/fernvilla.svg" alt=""/>
                        </div>
                        <!--name end-->

                        <!--divider-->
                        <div class="single-divider">
                          <img src="img/home/our-villas/villa-divider.svg" alt=""/>
                        </div>
                        <!--divider end-->

                        <!--single-tag-->
                        <div class="single-tag">
                          <h3 class="text-center text-uppercase">It's a palace, your Hightness!</h3>
                        </div>
                        <!--single-yag end-->

                        <!--detail-->
                        <div class="single-detail">
                          <p>5 Bed  |  G + 2  |  4640 Sq. Ft.</p>
                        </div>
                        <!--detail end-->

                        <div class="single-btn">
                          <a class="cta">Explore More<i class="fa fa-chevron-right"></i></a>
                        </div>
                      </div>
                      <!--box end-->
                    
                    </div>
                  </div>
                  <!--single-content end-->
                </div>

                <div class="single-wrap">
                  <div class="single-image">
                    <img src="img/home/our-villas/fernvilla.jpg" class="desk-img" alt=""/>
                    <img src="https://dummyimage.com/768x500/" class="mob-img" alt=""/>
                  </div>
                  <!--single-content-->
                  <div class="single-content">
                    <div class="flex full-h">
                    
                      <!--box-->
                      <div class="single-dbox">
                        <!--name-->
                        <div class="single-name">
                          <img src="img/home/our-villas/fernvilla.svg" alt=""/>
                        </div>
                        <!--name end-->

                        <!--divider-->
                        <div class="single-divider">
                          <img src="img/home/our-villas/villa-divider.svg" alt=""/>
                        </div>
                        <!--divider end-->

                        <!--single-tag-->
                        <div class="single-tag">
                          <h3 class="text-center text-uppercase">It's a palace, your Hightness!</h3>
                        </div>
                        <!--single-yag end-->

                        <!--detail-->
                        <div class="single-detail">
                          <p>5 Bed  |  G + 2  |  4640 Sq. Ft.</p>
                        </div>
                        <!--detail end-->

                        <div class="single-btn">
                          <a class="cta">Explore More<i class="fa fa-chevron-right"></i></a>
                        </div>
                      </div>
                      <!--box end-->
                    
                    </div>
                  </div>
                  <!--single-content end-->
                </div>

              </div>  
            </div>

          </div>
        </section>

        <section class="section-divide">
          <div class="section-wrapper">
            <div class="sect-title text-center">
              <h2 class="text-uppercase">Clubhouse</h2>
              <p>Inspired by architecture from around the world, the clubhouse has been built with thoughtful detail to design to give our valued luxury residents a world-class experience.</p>
            </div>
          </div>
          <div class="club-box">
            <div class="full-width">
              <img src="img/home/clubhouse/club-banner.jpg" class="desk-img" alt=""/>
              <img src="https://dummyimage.com/768x500/" class="mob-img" alt=""/>
            </div>
            <div class="single-dbox">
              <div class="single-tag">
                <img src="img/curve-left.svg" alt=""/>
                <h3 class="text-uppercase text-center">It's a better</h3>
                <img src="img/curve-right.svg" alt=""/>
              </div>
              <div class="single-divider">
                <h2 class="text-uppercase">Experience</h2>
              </div>
              <div class="single-tag last-tag">
                <h3 class="text-center text-uppercase">At every visit</h3>
              </div>
              <div class="single-btn">
                <a class="cta" tabindex="0">Explore More<i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </section>

        <section class="section-divide attract">
          <div class="section-wrapper">
            <div class="sect-title text-center">
              <h2 class="text-uppercase">Attractions</h2>
              <p>A quaint and charming town tucked away in South Gujarat, Valsad offers a wholesome staying experience, letting you savour life in a serene and green environment. Our tours are thoughtfully tailor-made to give you a glimpse of life at Valsad.</p>
            </div>
          </div>
          <div class="container">
            <div class="attract-item">

              <!--Looping-->
              <div class="col-md-4 single-wrap">
                <div class="single-image">
                  <img src="https://dummyimage.com/540x400/" alt=""/>
                </div>
                <div class="attract-content">
                  <h4>Soul of Valsad - a spiritual journey 1</h4>
                  <p>A city of spirituality, Valsad ia all-inclusive of all cultures and religions. Our spiritual
                  <span class="moretext">
                      Brisket ball tip cow sirloin. Chuck porchetta kielbasa pork chop doner sirloin, bacon beef brisket ball tip short ribs. 
                  </span>
                  <span class="moreless-button text-uppercase">Read more</span>
                  </p>
                </div>
              </div>
              <!--Looping end-->

              <div class="col-md-4 single-wrap">
                <div class="single-image">
                  <img src="https://dummyimage.com/540x400/" alt=""/>
                </div>
                <div class="attract-content">
                  <h4>Soul of Valsad - a spiritual journey 1</h4>
                  <p>A city of spirituality, Valsad ia all-inclusive of all cultures and religions. Our spiritual
                  <span class="moretext">
                      Brisket ball tip cow sirloin. Chuck porchetta kielbasa pork chop doner sirloin, bacon beef brisket ball tip short ribs. 
                  </span>
                  <span class="moreless-button text-uppercase">Read more</span>
                  </p>
                </div>
              </div>
              <div class="col-md-4 single-wrap">
                <div class="single-image">
                  <img src="https://dummyimage.com/540x400/" alt=""/>
                </div>
                <div class="attract-content">
                <h4>Soul of Valsad - a spiritual journey 1</h4>
                  <p>A city of spirituality, Valsad ia all-inclusive of all cultures and religions. Our spiritual
                  <span class="moretext">
                      Brisket ball tip cow sirloin. Chuck porchetta kielbasa pork chop doner sirloin, bacon beef brisket ball tip short ribs. 
                  </span>
                  <span class="moreless-button text-uppercase">Read more</span>
                  </p>
                </div>
              </div>
              <div class="col-md-4 single-wrap">
                <div class="single-image">
                  <img src="https://dummyimage.com/540x400/" alt=""/>
                </div>
                <div class="attract-content">
                <h4>Soul of Valsad - a spiritual journey 1</h4>
                  <p>A city of spirituality, Valsad ia all-inclusive of all cultures and religions. Our spiritual
                  <span class="moretext">
                      Brisket ball tip cow sirloin. Chuck porchetta kielbasa pork chop doner sirloin, bacon beef brisket ball tip short ribs. 
                  </span>
                  <span class="moreless-button text-uppercase">Read more</span>
                  </p>
                </div>
              </div>
            
            </div>  
          </div>
        </section>

        <section class="section-divide">
          <div class="section-wrapper container testimonial">
            <div class="row">
              <!--left side-->
              <div class="col-lg-6">
                <div class="sect-title">
                  <h2 class="text-uppercase">Testimonials</h2>
                  <p>At Mangalyam Meadows, we commit to offering luxury to our clients. Don't just take it from us; let our customers do all the talking.</p>
                </div>

                <div class="testimonial-item">
                  <!--Looping-->
                  <div>
                    <div class="cards">
                      <div class="card-content">
                        <p class="text-left">Thanks to the tranquility & peace in this project, it refreshes and rejuvenates us on every family weekend trip.</p>
                      </div>
                      <div class="single-divider">
                        <img src="img/home/test-divider.png" alt=""/>
                      </div>

                      <div class="user-detail">
                        <div class="user-name">
                          <img src="img/home/user-avatar.png" alt=""/>
                          <span>- Jane Doe</span>
                        </div>
                        <div class="user-rate">
                          <img src="img/home/ratings.svg" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--Looping end-->

                  <div>
                    <div class="cards">
                      <div class="card-content">
                        <p class="text-left">Thanks to the tranquility & peace in this project, it refreshes and rejuvenates us on every family weekend trip.</p>
                      </div>
                      <div class="single-divider">
                        <img src="img/home/test-divider.png" alt=""/>
                      </div>

                      <div class="user-detail">
                        <div class="user-name">
                          <img src="img/home/user-avatar.png" alt=""/>
                          <span>- Jane Doe</span>
                        </div>
                        <div class="user-rate">
                          <img src="img/home/ratings.svg" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="cards">
                      <div class="card-content">
                        <p class="text-left">Thanks to the tranquility & peace in this project, it refreshes and rejuvenates us on every family weekend trip.</p>
                      </div>
                      <div class="single-divider">
                        <img src="img/home/test-divider.png" alt=""/>
                      </div>

                      <div class="user-detail">
                        <div class="user-name">
                          <img src="img/home/user-avatar.png" alt=""/>
                          <span>- Jane Doe</span>
                        </div>
                        <div class="user-rate">
                          <img src="img/home/ratings.svg" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

              </div>
              <!--left side end-->

              <!--Right side-->
              <div class="col-lg-6">
                <div class="Responsive-Ratio Dynamic Fit-Content video_wrapper video_wrapper_full js-videoWrapper">
                  <video width="100%" height="935" controls="" id="player1" class="videoIframe js-videoIframe">
                    <source src="movie.mp4" type="video/mp4">
                    <source src="movie.ogg" type="video/ogg">
                    Your browser does not support the video tag.
                  </video>
                  <!-- <iframe id="player1" class="videoIframe js-videoIframe" src="https://www.youtube.com/embed/mxYNdy-0CEs?rel=0&modestbranding=1&autohide=1&showinfo=0&autoplay=1&mute=1&controls=0" width="720" height="400" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allowTransparency="true" data-src="https://www.youtube.com/embed/mxYNdy-0CEs?rel=0&modestbranding=1&autohide=1&showinfo=0&autoplay=1&mute=1&controls=0"></iframe> -->
                  <button class="videoPoster js-videoPoster"></button>
                </div>
              </div>
              <!--Right side end-->

            </div>
          </div>
        </section>

    </main> 
    <!--main-container end-->
    

    <?php include 'includes/footer.php'; ?>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                                                              function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                             e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                             e.src='//www.google-analytics.com/analytics.js';
                             r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
  </body>
</html>
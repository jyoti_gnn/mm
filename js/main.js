$(document).ready(function($) {
    $(".hamburger").click(function(){
        $(this).toggleClass("is-active");
    });
    $(".scrollto").click(function() {
        $('html, body').animate({
            scrollTop: $("#scroll-sect").offset().top - 100
        }, 2000);
    });
    $(window).scroll(function() {  
        //if($(window).width() > 991) {
            var scroll = $(window).scrollTop();
            //console.log(scroll);
            if (scroll >= 50) {
                //console.log('a');
                $("header").addClass("change");
            } else {
                //console.log('a');
                $("header").removeClass("change");
            }
        //}
    });
    
    if($(window).width() <= 575) {
        $('.foot-title').click(function(e) {
            e.preventDefault();

            var $this = $(this);
            // $this.toggleClass("foot-accordian");
            
            if ( $this.hasClass('foot-accordian') ) {
                $this.removeClass('foot-accordian');
            } else {
                $('.foot-title.foot-accordian').removeClass('foot-accordian');
                $(this).addClass('foot-accordian');    
            }

            if ($this.next().hasClass('show')) {
                $this.next().removeClass('show');
                $this.next().slideUp(350);
            } else {
                $this.parent().parent().find('.tab-content').removeClass('show');
                $this.parent().parent().find('.tab-content').slideUp(350);
                $this.next().toggleClass('show');
                $this.next().slideToggle(350);
            }
        });
    }

    if($(window).width() > 991) {
        $(".parent-drop").hover(function () {
            $('.parent-drop').toggleClass("drop-active");
        });
    }

    if($(window).width() <= 991) {
        $(".parent-drop a").click(function(){
            $(".parent-drop .dropdown").toggleClass('slow');
            $(".parent-drop").toggleClass('drop-active');
        });
    }

    var $win = $(window),
        $head = $("head"),
        $root = $("body");
    
    var ratios = {},
        ratioStyles = '';
    
    $('.Responsive-Ratio.Dynamic').each(function(i){
        var $this = $(this),
        $video = $this.children().first(),
        width = ( $video.attr("width") || $video.width() ),
        height = ( $video.attr("height") || $video.height() ),
        ratio = ((height / width)*100),
        className = "Ratio_"+Math.floor(ratio);
        console.log("width",width,"height",height,"ratio",ratio);
      
        ratios[className] = ratio;
        $this.addClass(className);
    });
    
    //console.log("ratios",ratios);
    
    $.each(ratios, function(key, value) {
      ratioStyles += '.Dynamic.' + key + ':after { padding-bottom: '+value + '%; }\n';
    });
    
    //console.log("ratioStyles",ratioStyles);
    $head.append("<style>"+ratioStyles+"</style>");

    
    
    $(document).on('click','.js-videoPoster',function(e) {
        e.preventDefault();
        var poster = $(this);
        $('.video-sect video').get(0).play();
        $('.video-sect .single-dbox').hide();
        var wrapper = poster.closest('.js-videoWrapper');
        videoPlay(wrapper);
    });

    $(document).on('click','.testimonial .js-videoPoster',function(e) {
        e.preventDefault();
        var poster = $(this);
        $('.testimonial video').get(0).play();
        var wrapper = poster.closest('.js-videoWrapper');
        videoPlay(wrapper);
    });
      
    function videoPlay(wrapper) {
        var iframe = wrapper.find('.js-videoIframe');
        
        var src = iframe.data('src');
       
        wrapper.addClass('videoWrapperActive');
        
        iframe.attr('src',src);
    }
    
    $('.contact-form input, .contact-form textarea, .contact-form label').on('focusin', function() {
        $(this).parent().find('label').addClass('active');
    });
      
    $('.contact-form input, .contact-form textarea, .contact-form label').on('focusout', function() {
        if (!this.value) {
          $(this).parent().find('label').removeClass('active');
        }
    });

    $(".list-thumb img").click(function(e) {
        e.preventDefault();
        $this = $(this);
        $(".listing-banner").css("background-image", "url(" + $this.parent().data("bg") + ")");
    });

    
    $('.moreless-button').click(function() {
        $(this).prev().slideToggle();
        if ($(this).text() == "Read more") {
          $(this).text("Read less")
        } else {
          $(this).text("Read more")
        }
    });



    $("#form").validate({
        rules: {
            name: {
                required: true                
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                maxlength: 10,
                minlength: 10
            },
            city: {
                required: true                
            },
            message: {
                required: true                
            }
        },
        messages: {
            name: {
                required: "Please enter your name"
            },
            email: {
                required: "Please enter your email address.",
                email: "Email is invalid"
            },
            phone: {
                required: "Please enter phone no.",
                minlength: "Please enter 10 digit number.",                
                maxlength: "Please enter 10 digit number."
            },
            city: {
                required: "Please enter your city"
            },
            message: {
                required: "Please enter your message"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $("#bookavisit").validate({
        rules: {
            bavname: {
                required: true                
            },
            bavemail: {
                required: true,
                email: true
            },
            bavphone: {
                required: true,
                maxlength: 10,
                minlength: 10
            }
        },
        messages: {
            bavname: {
                required: "Please enter your name"
            },
            bavemail: {
                required: "Please enter your email address.",
                email: "Email is invalid"
            },
            bavphone: {
                required: "Please enter phone no.",
                minlength: "Please enter 10 digit number.",                
                maxlength: "Please enter 10 digit number."
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    
    $("#subs").validate({
        rules: {
            phone: {
                required: true,
                maxlength: 10,
                minlength: 10
            }
        },
        messages: {
            phone: {
                required: "Please enter phone no.",
                minlength: "Please enter 10 digit number.",                
                maxlength: "Please enter 10 digit number."
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $("#download").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Email is invalid"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });



    $('.single-item').slick({
        dots: true,
        infinite: true,
        arrows: false,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
      });
      $('.attract-item').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        slidesToShow: 3,
        adaptiveHeight: true,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
      $('.testimonial-item').slick({
        infinite: false,
        dots: true,
        arrows: false,
        autoplay: false,
        swipe: true,
        touchMove: false,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 1.4,
        slidesToScroll: 1,
        lazyLoad: 'progressive',
        responsive: [
          {
              breakpoint: 991,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  centerMode: false
              }
          },
          {
              breakpoint: 500,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  centerMode: false
              }
          },
        ]
      });
      

    $("#datepicker").datepicker({
    showOtherMonths: true,
    selectOtherMonths: true,
    minDate: 0,
    maxDate: "+2M",
    dateFormat: "D, MM d, yy", 
        onSelect: function(){
            var selectedDate = $(this).val();
            $(".selectDate li").removeClass("disable");
        }
    });
    
    
    //Get Time
    $(document).on('click','.selectDate li', function(e){
    
    var selectedTime = $(this).attr('data-attr');
    
    $('ul li.current').removeClass('current');
    
    $(this).addClass('current');
    $('#confirmSlot').removeClass('removePointer');
    
    });
    
    //Get Date & Time
    
    $('#confirmSlot').click(function(){
    
    var Currentdate = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    var Currenttime = $('.current').attr('data-attr');		
    
    $(".print-date").text(Currentdate);
    $(".print-time").text(Currenttime);
    $('.bav_form_box').addClass('openForm');
    });
    
    $('.back_btn').click(function(){
        $('.bav_form_box').removeClass('openForm');
    });
    
    

});